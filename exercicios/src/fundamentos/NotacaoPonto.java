package fundamentos;

public class NotacaoPonto {
	public static void main(String[] args) {
		String s = "Bom dia X";
		s = s.replace("X", "Senhora");
		s = s.toUpperCase();
		
		s = s.concat("!!!!");

		System.out.println(s);
		System.out.println("Fabio".toUpperCase());
		
		String x = "Fulano".toUpperCase();
		System.out.println(x);
		
		// Tipos primitivos n�o tem o operador "."
		// n�o da pra usar com int, float..etc
	}
}
