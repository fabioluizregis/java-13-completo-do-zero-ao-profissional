package fundamentos;

/**
 * Dois asteriscos para realizar documenta��o
 * 
 * @author R�gis
 *
 */
public class PrimeiroPrograma {
	public static void main(String[] args) {

		// Coment�rio de uma linha
		System.out.println("Primeiro programa Parte #01!");

		/*
		 * Coment�rio para v�rias linhas
		 */
		System.out.println("Primeiro programa Parte #02!");
	}
}
