package fundamentos;

import java.util.Scanner;

public class DesafioConversao {
	public static void main(String[] args) {
//		a partir do scanner
//		pegar 3 string usando next line
//		cada string pega os ultimos 3 salarios
//		calcular a media dos ultimos 3 salarios
//		Neste salario, o funcionario pode separar
//		as casas decimais com virgula ou ponto
		
		System.out.println("Vamos calcular a media dos seus �ltimos 3 sal�rios!!");
		
		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite seu primeiro sal�rio: ");
		String primeiroSalario = entrada.nextLine().replace(",", ".");		
		System.out.println("Digite seu segundo sal�rio: ");
		String segundoSalario = entrada.nextLine().replace(",", ".");
		
		System.out.println("Digite seu terceiro sal�rio: ");
		String terceiroSalario = entrada.nextLine().replace(",", ".");
		
		Double salario1 = Double.parseDouble(primeiroSalario);
		Double salario2 = Double.parseDouble(segundoSalario);
		Double salario3 = Double.parseDouble(terceiroSalario);

		
		Double media = (salario1 + salario2 + salario3) / 3;
		
		System.out.println("Sua m�dia salarial dos �ltimos 3 sal�rios �: " + media);
		
		entrada.close();	
	}
}
