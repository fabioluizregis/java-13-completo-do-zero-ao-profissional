package fundamentos;

import java.util.Scanner;

public class DesafioCalculadora {
	public static void main(String[] args) {
		// Ler num1
		// Ler num2
		// Ler operacao
		// + - * / %
		// Mostrar resultado
		// Usar s� o que foi ensinado at� o momento
		Scanner entradaDeDados = new Scanner(System.in);
		System.out.println("Bem vindo a nossa calculadora!");
		System.out.println("Qual a opera��o que deseja realizar?");
		System.out.print("Digite apenas o s�mbolo (+, -, *, /, % (para m�dulo)): ");
		String operacao = entradaDeDados.next();
		
		String soma = new String(operacao.trim());
		String subtracao = new String(operacao.trim());
		String multiplicacao = new String(operacao.trim());
		String divisao = new String(operacao.trim());
		
		System.out.print("Agora digite o primeiro numero: ");
		String primeiroNumero = entradaDeDados.next();
		
		System.out.print("Agora digite o primeiro numero: ");
		String segundoNumero = entradaDeDados.next();
		
		double n1 = Double.parseDouble(primeiroNumero);
		double n2 = Double.parseDouble(segundoNumero);
		
		double resultado = "+".equals(soma) ? n1 + n2 : 
			               "-".equals(subtracao) ? n1 - n2 :
			               "*".equals(multiplicacao) ? n1 * n2 :
			               "/".equals(divisao) ? n1 / n2 : n1 % n2; // se n�o for divisao � modulo
		
		System.out.println("O resultado da operacao " + primeiroNumero + " " + operacao + " " + segundoNumero + " �: " + resultado);
		
		entradaDeDados.close();
	}
}
