package fundamentos.operadores;

public class DesafioLogicos {
	public static void main(String[] args) {
		// Trabalho na ter�a (V ou F)
		// Trabalho na quinta (V ou F)
		
//		1 trab 32
//		2 trab 50
//		comprou = toma sorvete
//		1 ou 2 nao = fica em casa
		
		boolean trabalhoTerca = true;
		boolean trabalhoQuarta = true;
				
		System.out.println("Compra TV 32?");
		System.out.println(trabalhoTerca || trabalhoQuarta);
		System.out.println(!trabalhoTerca || trabalhoQuarta);
		System.out.println(trabalhoTerca || !trabalhoQuarta);
		System.out.println(!trabalhoTerca || !trabalhoQuarta);
		
		System.out.println("Compra TV 50?");
		System.out.println(trabalhoTerca && trabalhoQuarta);
		System.out.println(!trabalhoTerca && trabalhoQuarta);
		System.out.println(trabalhoTerca && !trabalhoQuarta);
		System.out.println(!trabalhoTerca && !trabalhoQuarta);
		
		System.out.println("Compra Sorvete?");
		System.out.println(trabalhoTerca && trabalhoQuarta);
		System.out.println(!trabalhoTerca && trabalhoQuarta);
		System.out.println(trabalhoTerca && !trabalhoQuarta);
		System.out.println(!trabalhoTerca && !trabalhoQuarta);
		System.out.println(trabalhoTerca || trabalhoQuarta);
		System.out.println(!trabalhoTerca || trabalhoQuarta);
		System.out.println(trabalhoTerca || !trabalhoQuarta);
		System.out.println(!trabalhoTerca || !trabalhoQuarta);
	}
}
