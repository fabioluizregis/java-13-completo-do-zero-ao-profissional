package fundamentos.exercicios;

import java.util.Scanner;

public class CalcudoDoQuadradoECubo {
	//	Criar um programa que leia um valor e apresente os resultados ao quadrado e ao cubo do valor.
	
	public static void main(String[] args) {
		Scanner entradaValor = new Scanner(System.in);
			System.out.println("Digite um numero: ");
			double numero = entradaValor.nextDouble();
			
			System.out.println(numero + " ao quadrado = " + Math.pow(numero, 2));
			System.out.println(numero + " ao cubo = " + Math.pow(numero, 3));
		entradaValor.close();
	}
}
