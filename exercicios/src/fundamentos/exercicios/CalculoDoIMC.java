package fundamentos.exercicios;

import java.util.Scanner;

public class CalculoDoIMC {
	//	Criar um programa que leia o peso e a altura do usu�rio e imprima no console o IMC.
	
	// F�rmula IMC = Massa / Altura ^ 2
	
	public static void main(String[] args) {
		Scanner entradaDeDados = new Scanner(System.in);
			System.out.println("Digite seu peso em Kg (ex: 75,4): ");
			double massa = entradaDeDados.nextDouble();
			
			System.out.println("Digite sua altura em metros (ex: 1,75) : ");
			double altura = entradaDeDados.nextDouble();
			
			double imc = massa / Math.pow(altura, 2);
			
			System.out.println("Seu IMC equivale a: " + imc);
			
		
		entradaDeDados.close();
	}
}
