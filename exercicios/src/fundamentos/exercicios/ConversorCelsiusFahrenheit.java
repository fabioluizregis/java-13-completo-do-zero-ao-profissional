package fundamentos.exercicios;

import java.util.Scanner;

public class ConversorCelsiusFahrenheit {
	//  Criar um programa que leia a temperatura em Celsius e converta para Fahrenheit.
	
	// Formula �F = �C � 1,8 + 32

	public static void main(String[] args) {
		
		Scanner entradaDeDados = new Scanner(System.in);
		
		System.out.println("Digite a temperatura em �C: ");
		double temperaturaCelsius = entradaDeDados.nextDouble();
		
		double temperaturaFahrenheit = temperaturaCelsius * 1.8 + 32;
		
		System.out.println(temperaturaCelsius + "�C equivalem a " + temperaturaFahrenheit + "�F");
		
		
		
		entradaDeDados.close();			
	}
}
