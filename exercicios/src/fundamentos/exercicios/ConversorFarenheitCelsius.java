package fundamentos.exercicios;

import java.util.Scanner;

public class ConversorFarenheitCelsius {
	// Criar um programa que leia a temperatura em Celsius e converta para
	// Fahrenheit.

	// Formula °C = (°F − 32) ÷ 1, 8
	public static void main(String[] args) {
			
			Scanner entradaDeDados = new Scanner(System.in);
			
			System.out.println("Digite a temperatura em ºF: ");
			double temperaturaFahrenheit = entradaDeDados.nextDouble();
			
			double temperaturaCelsius = (temperaturaFahrenheit - 32) / 1.8;
			
			System.out.println(temperaturaFahrenheit + "ºF equivalem a " + temperaturaCelsius + "ºC");
			
			
			
			entradaDeDados.close();

	}
}