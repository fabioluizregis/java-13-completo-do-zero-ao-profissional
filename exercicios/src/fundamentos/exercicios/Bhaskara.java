package fundamentos.exercicios;

import java.util.Scanner;

public class Bhaskara {
	//  Criar um programa que resolve equa��es do segundo grau (ax2 + bx + c = 0) utilizando a f�rmula de Bhaskara. 
	//  Use como exemplo a = 1, b = 12 e c = -13. Encontre o delta
	
	public static void main(String[] args) {
		Scanner entradaDeDados = new Scanner(System.in);
			System.out.println("Resolvendo equa��o de 2� grau!");
			
			System.out.println("Digite o valor de a: ");
			double a = entradaDeDados.nextDouble();
			
			System.out.println("Digite o valor de b: ");
			double b = entradaDeDados.nextDouble();
			
			System.out.println("Digite o valor de c: ");
			double c = entradaDeDados.nextDouble();
			
			// delta = b^2 - 4ac
			double delta = Math.pow(b,2) - (4 * a * c);
			System.out.println("Delta = " + delta);
			
		entradaDeDados.close();
	}
}
