package fundamentos.exercicios;

import java.util.Scanner;

public class AreaDoTriangulo {
	//  Criar um programa que leia o valor da base e da altura de um tri�ngulo e calcule a �rea.
	// Area do triangulo = ( base * altura ) / 2
	
	public static void main(String[] args) {
		Scanner entradaDeDados = new Scanner(System.in);
			System.out.println("C�lculo da �rea do tri�ngulo!!");
			
			System.out.println("Digite o valor da base em cm: ");
			double base = entradaDeDados.nextDouble();
			
			System.out.println("Digite o valor da altura em cm: ");
			double altura = entradaDeDados.nextDouble();
			
			System.out.println("A �rea do tri�ngulo �: " + ((base * altura) / 2) + "cm^2");
		entradaDeDados.close();
	}
}
