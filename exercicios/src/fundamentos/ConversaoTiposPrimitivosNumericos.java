package fundamentos;

public class ConversaoTiposPrimitivosNumericos {
	public static void main(String[] args) {

		double a = 1; // Convers�o impl�cita
		System.out.println(a);
		
//		float b = 1.12345F; //tamb�m funciona
		float b = (float) 1.12345; //Convers�o expl�cita (CAST)
		System.out.println(b);
		
		int c = 4;
		byte d = (byte) c;  //Convers�o expl�cita (CAST)
		System.out.println(d);
		
		double e = 1;
		int f = (int) e;
		System.out.println(f);
	}
}
