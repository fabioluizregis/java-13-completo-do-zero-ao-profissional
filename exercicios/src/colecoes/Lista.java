package colecoes;

import java.util.ArrayList;

public class Lista {
	public static void main(String[] args) {
		ArrayList<Usuario> lista = new ArrayList<Usuario>();
		
		Usuario u1 = new Usuario("Ana");
		
		lista.add(u1);
		lista.add(new Usuario("Carlos"));
		lista.add(new Usuario("Lia"));
		lista.add(new Usuario("Bia"));
		lista.add(new Usuario("Manu"));
		
		// Acessando pelo �ndice!
		System.out.println(lista.get(3)); //Imprime o nome por conta do m�todo String na classe Usuario.
		
		lista.remove(1); //Remove o Carlos pelo �ndice
		
		lista.remove(new Usuario("Manu")); //Remove pelo nome
		
		System.out.println(lista.contains(new Usuario("Lia"))); //Verifica se a Lia existe.
		
		for(Usuario u: lista) {
			System.out.println(u.nome);
		}
	}
}
