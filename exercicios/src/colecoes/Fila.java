package colecoes;

import java.util.LinkedList;
import java.util.Queue;

public class Fila {
	public static void main(String[] args) {
		
		Queue<String> fila = new LinkedList<String>();
				
		// Offer e Add -> adicionam elementos na fila
		// Diferen�a � o comportamento quando a fila est� cheia
		fila.add("Ana");	//adiciona na fila e se n�o conseguir gera um erro
		fila.offer("Bia");  //adiciona na fila e retorna true ou false caso consiga inserir
		fila.offer("Carlos");
		fila.offer("Daniel");
		fila.offer("Rafaela");
		fila.offer("Gui");
		
		// peek e element -> Retornam o prox�mo elemento da fila (sem remover)
		// Diferen�a est� no comportamento de quando a fila est� vazia.
		//retornar o primeiro elemento da fila
		System.out.println(fila.peek());
		System.out.println(fila.peek());
		System.out.println(fila.element());
		System.out.println(fila.element());
		
//		fila.size();
//		fila.clear();
//		fila.isEmpty();
//		fila.contains(object);
		
		// retornar o pr�ximo elemento da fila
		// removendo o elemento
		// poll e remove -> Diferen�a est� quando a fila est� vazia
		// poll retorna null
		// remove retorna uma exce��o
		System.out.println(fila.poll()); // Ana
		System.out.println(fila.remove()); // Bia
		System.out.println(fila.poll()); // Carlos
		System.out.println(fila.poll()); // Daniel
		System.out.println(fila.poll()); // Rafaela
		System.out.println(fila.poll()); // Gui
		System.out.println(fila.poll()); // NULL .. est� vazio
		
	}
}
