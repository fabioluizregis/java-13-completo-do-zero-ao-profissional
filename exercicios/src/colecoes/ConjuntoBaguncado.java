package colecoes;

import java.util.HashSet;
import java.util.Set;

public class ConjuntoBaguncado {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		
		HashSet conjunto = new HashSet();
		
		//Set n�o aceita tipos primitivos, ent�o ele converte automaticamente para classes
		conjunto.add(1.2); // double -> Double
		conjunto.add(true); //boolean -> Boolean
		conjunto.add("Teste"); // String
		conjunto.add(1); // int -> Integer
		conjunto.add('x');
		
		System.out.println("Tamanho: " + conjunto.size());
		
		conjunto.add("Teste"); // Set n�o aceita adicionar repeti��o de elementos
		System.out.println("Tamanho: " + conjunto.size());
		
		//Remover elemento do conjunto
		System.out.println(conjunto.remove("teste"));
		System.out.println(conjunto.remove("Teste"));
		System.out.println(conjunto.remove('x'));
		System.out.println("Tamanho: " + conjunto.size());
		
		System.out.println(conjunto.contains('x'));
		System.out.println(conjunto.contains(1));
		System.out.println(conjunto.contains(false));
		System.out.println(conjunto.contains(true));
		
		Set nums = new HashSet();
		
		nums.add(1);
		nums.add(2);
		nums.add(3);
		
		System.out.println(nums);
		System.out.println(conjunto);
		
//		conjunto.addAll(nums); // Uni�o entre conjuntos
		conjunto.retainAll(nums); // Interse��o entre conjuntos
		System.out.println(conjunto);
		
		conjunto.clear();
		System.out.println(conjunto);
		
	}
}
