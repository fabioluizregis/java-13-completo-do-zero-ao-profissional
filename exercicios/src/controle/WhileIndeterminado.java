package controle;

import java.util.Scanner;

public class WhileIndeterminado {
public static void main(String[] args) {
	Scanner entrada = new Scanner(System.in);
	
	String valor = "";
	
	while(!valor.equalsIgnoreCase("Sair")) {
		System.out.print("Digite a palavra secreta: ");
		valor = entrada.nextLine();
	}
	
	System.out.println("Parab�ns, voc� encontrou a palavra secreta!");
	entrada.close();
}
}
