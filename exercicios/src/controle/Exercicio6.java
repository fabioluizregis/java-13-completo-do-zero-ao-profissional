package controle;

import java.util.Scanner;

public class Exercicio6 {

//	Jogo da adivinhação: Tentar adivinhar um número entre 0 - 100. 
//	Armazene um numero aleatório em uma variável. 
//	O Jogador tem 10 tentativas para adivinhar o número gerado. 
//	Ao final de cada tentativa, imprima a quantidade de tentativas restantes, 
//	e imprima se o número inserido é maior ou menor do que o número armazenado.

	public static void main(String[] args) {
		System.out.println("Jogo da adivinhação!!");
		System.out.println("Adivinhe o número entre 0 a 100! em até 10 tentativas");

		Scanner entrada = new Scanner(System.in);
		int numeroDaSorte = 53;

		int numeroDigitado;
		int contador = 10;

		for (int i = 0; i < 10; i++) {

			System.out.println("Bem vindo(a)! Você tem " + contador + " tentativas restantes!");
			System.out.println("Digite o numero da sorte!: ");
			numeroDigitado = entrada.nextInt();

			if (numeroDigitado == numeroDaSorte) {
				i++;
				System.out.println("Parabéns!! Você acertou o número em " + i + " tentativas.");
				break;
			} else if (numeroDigitado < numeroDaSorte) {
				System.out.println("Quase! Esse número é menor que o número que deve adivinhar!");
			} else if (numeroDigitado > numeroDaSorte) {
				System.out.println("Quase! Esse número é maior que o número que deve adivinhar!");
			}
			
			contador--;
		}
		entrada.close();
	}
}
