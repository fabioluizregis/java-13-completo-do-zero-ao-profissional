package controle;

public class DesafioFor {
	public static void main(String[] args) {
		
		String valor = "#";
		for (int i = 1; i <= 5; i++) {
			System.out.println(valor);
			valor += "#";

		}
		
		// Desafio:
		// N�o pode usar valor num�rico para controlar o la�o
		String frase = "#";
		for (int j=frase.length(); j<"#####".length();frase+="#") {
			System.out.println(frase);
			j=frase.length();
		}
		
		//resposta professor
		for(String v = "#"; !v.equals("######"); v+= "#") {
			System.out.println(v);
		}
	}
}
