package controle;

import java.util.Scanner;

public class Exercicio1 {
	
	// Criar um programa que receba um número e 
	// verifique se ele está entre 0 e 10 e é par;
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
			
			System.out.print("Digite um número de 0 a 10: ");
			int numero = entrada.nextInt();
			if(numero >= 0 && numero <= 10) {
				if(numero % 2 == 0)
				{
					System.out.println("É par!");
				} 
				else {
					System.out.println("É impar!");
				}
				
			} else {
				System.out.println("Número fora do range!");
			}
		
		entrada.close();
	}
}
