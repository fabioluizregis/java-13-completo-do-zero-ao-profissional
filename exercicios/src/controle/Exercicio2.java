package controle;

import java.util.Scanner;

//Criar um programa informa se o ano atual é um ano bissexto;
public class Exercicio2 {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
			System.out.print("Digite o ano para saber se é bissexto: ");
			int ano = entrada.nextInt();
			
			if ( ano % 4 == 0 && ano % 100 != 0)
			{
				System.out.println("É ano bissexto!");
			}
			else {
				System.out.println("Não é bissexto!");
			}
		entrada.close();
	}
}
