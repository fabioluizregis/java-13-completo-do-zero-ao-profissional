package controle;

import java.util.Scanner;

public class Exercicio4 {
//	Criar um programa que receba um número e diga se ele é um número primo.
	
	
//	Para determinarmos se um dado número é primo, precisamos verificar sua divisibilidade inteira. 
//	Para isso, podemos realizar divisões sucessivas do número dado por todos os números 
//	a partir de 2 até a metade dele próprio, verificando o resto da divisão. 
//	Se o resto for 0 em algum momento, significa que houve uma divisão inteira 
//	por um valor diferente de 1 e do número em si, e o número não é primo. 
//	Caso contrário, se o resto for diferente de 0 em todas as divisões, o número é primo.
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		System.out.print("Digite um número inteiro para saber se ele é primo: ");
		int numero = entrada.nextInt();

		int resultado = 0;

		for (int i = 2; i <= numero / 2; i++) {
			if (numero % i == 0) {
				resultado++;
				break;
			}
		}
		
		if (resultado == 0)
		{
			System.out.println("É primo!");
		}
		else
		{
			System.out.println("Não é primo!");
		}
		
		entrada.close();
	}
}
