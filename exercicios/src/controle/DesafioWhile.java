package controle;

import java.util.Scanner;

public class DesafioWhile {
public static void main(String[] args) {
//	calcular media de notas da turma
//	ver se � valida
//	var = total
//	numero de notas validas ++
//	para sair -1
//	se nota invalida - avisar pra digitar novamente
	
	Scanner entrada = new Scanner(System.in);
		System.out.println("Bem vindo ao sistema de c�lculo de m�dias da turma!");
		
		double nota = 0;
		int contador_notas_validas = 0;
		double total = 0;
		
		
		do{
			contador_notas_validas ++;
			System.out.print("Digite a " + contador_notas_validas + "� nota: " );
			nota = entrada.nextDouble();
			if (nota == -1) {
				contador_notas_validas -= 1;
				break;
				}
			
			while(nota<0 || nota>10)
			{
				System.out.print("Nota inv�lida! Ditige uma nota v�lida de 0 a 10 : ");
				nota = entrada.nextDouble();
				
				if (nota == -1) {
					contador_notas_validas -= 1;
					break;	
				}		
			}
			total += nota;			
		}while (nota != -1);
		
		if (contador_notas_validas == 0)
		{
			total = 0;
			System.out.println("N�o tivemos notas cadastradas!");
		}
		else {
			System.out.println("A media das notas da turma foi: " + total/contador_notas_validas + " de um total de " + contador_notas_validas + " notas cadastradas!");
		}
		
		
			
	entrada.close();
}
}
