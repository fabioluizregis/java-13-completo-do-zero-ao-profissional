package controle;

import java.util.Scanner;

public class Exercicio7 {
//	Criar um programa que enquanto estiver recebendo n�meros positivos, 
//	imprime no console a soma dos n�meros inseridos, caso receba um n�mero negativo, encerre o programa. 
//	Tente utilizar a estrutura do while.

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);

		double numero = 0;
		int soma = 0;

		while (numero >= 0) {
			System.out.print("Digite um numero para ser somado: ");
			numero = entrada.nextDouble();
			
			if (numero < 0)
			{
				System.out.println("Voc� digitou um numero negativo! TCHAU!!");
				break;
			}
			soma += numero;
			
			System.out.println("Soma = " + soma);
		}
		

		entrada.close();
	}
}
