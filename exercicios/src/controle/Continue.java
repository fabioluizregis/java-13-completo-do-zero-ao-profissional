package controle;

public class Continue {
	public static void main(String[] args) {
		
		for (int i = 0; i < 10; i++) {
			
			if (i % 2 == 1)
			{
				continue;
				// interrompe a itera��o e vai pra pr�xima.
			}
			System.out.println(i);
		}
		
		System.out.println("========== Exercicio 2 ==========");
		
		for (int i = 0; i < 10; i++) {
			
			if (i == 5)
				continue; //n�o imprime o 5
			
			System.out.println(i);
		}
	}
}
