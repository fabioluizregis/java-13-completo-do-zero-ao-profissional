package controle;

import java.util.Scanner;

public class DesafioDiaSemana {
	public static void main(String[] args) {
		// Domingo --> 1
		// Segue at� S�bado --> 7
		// Imprimir o numero do dia da semana digitado
		Scanner diaDaSemana = new Scanner(System.in);
		
		System.out.print("Digite o dia da semana: ");
		String dia = diaDaSemana.nextLine();
		
		if (dia.equalsIgnoreCase("domingo"))
		{
			System.out.println("1");
		} else if (dia.equalsIgnoreCase("segunda"))
		{
			System.out.println("2");
		} else if (dia.equalsIgnoreCase("ter�a") || dia.equalsIgnoreCase("terca"))
		{
			System.out.println("3");
		} else if (dia.equalsIgnoreCase("quarta"))
		{
			System.out.println("4");
		} else if (dia.equalsIgnoreCase("quinta"))
		{
			System.out.println("5");
		} else if (dia.equalsIgnoreCase("sexta"))
		{
			System.out.println("6");
		} else if (dia.equalsIgnoreCase("s�bado") || dia.equalsIgnoreCase("sabado"))
		{
			System.out.println("7");
		} else
		{
			System.out.println("Entrada inv�lida!!!");
			System.out.println("Entradas permitidas: Domingo, Segunda, Ter�a, Quarta, Quinta, Sexta e S�bado!");
		}

		System.out.println("\n\nFim");
		diaDaSemana.close();
	}
}
