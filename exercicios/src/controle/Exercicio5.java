package controle;

import java.util.Scanner;

//Refatorar o exercício 04, utilizando a estrutura switch.
public class Exercicio5 {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		System.out.print("Digite um número inteiro para saber se ele é primo: ");
		int numero = entrada.nextInt();

		int resultado = 0;

		for (int i = 2; i <= numero / 2; i++) {
			if (numero % i == 0) {
				resultado++;
				break;
			}
		}

		switch (resultado) {
		case 0: {
			System.out.println("É primo!");
			break;
		}
		default: {
			System.out.println("Não é primo!");
		}
		}

		entrada.close();
	}
}
