package controle;

import java.util.Scanner;

public class Exercicio9 {
//	Crie um programa que recebe 10 valores e ao final imprima o maior n�mero.
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Voc� ir� digitar 10 valores e eu te direi qual o maior deles.");
		
		System.out.print("Digite o 1� valor : ");
		double maior = entrada.nextDouble();
		double valor;
		
		int i = 1;
		while(i != 10)
		{
			i++;
			System.out.print("Digite o " + i + "� valor : ");
			valor = entrada.nextDouble();
			
			if (valor > maior)
			{
				maior = valor;
			}
		}
		
		System.out.println("O maior valor digitado foi: " + maior);
		entrada.close();
	}
}
