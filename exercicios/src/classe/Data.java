package classe;

public class Data {
	// Desafio 1:
	// Criar a classe Data, com 3 atributos. Dia, M�s e Ano.
	int dia;
	int mes;
	int ano;
	
	//construtor padr�o
	Data() {
//		dia = 01;
//		mes = 01;
//		ano = 1970;
		this(1, 1, 1970); //Referencia um construtor dentro da classe.
						  //O que define qual o construtor � a quandidade e ordem dos par�metros.
	}
	
	//Novo construtor para receber dia, m� e ano
	Data(int diaInicial, int mesInicial, int anoInicial) {
		this.dia = dia;  //this referencia o objeto que est� sendo criado naquele momento.
		this.mes = mes;  //this referencia a vari�vel de classe. L��� em cima!
		this.ano = ano;  //evita conflito de nomes de vari�veis.
	}
	
	String obterDataFormatada(int dia, int mes, int ano) {
		
		String dataNoFormato = dia + "/" + mes + "/" + ano;
		return dataNoFormato;	
	}
	
	String obterDataFormatada() {
		
		return String.format("%d/%d/%d", dia, mes, ano);	
	}
}
