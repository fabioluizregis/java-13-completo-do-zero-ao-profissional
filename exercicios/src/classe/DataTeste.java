package classe;

public class DataTeste {
	// Desafio 1:
	// Instanciar 2 datas, utilizando a classe Data criada.
	public static void main(String[] args) {
		Data data1 = new Data();
		Data data2 = new Data(02, 04, 2011);	
		
//		data1.dia = 17;
//		data1.mes = 10;
//		data1.ano = 1979;
		
//		data2.dia = 02;
//		data2.mes = 04;
//		data2.ano = 2011;
		
		System.out.println(data1.obterDataFormatada(data1.dia, data1.mes, data1.ano));
		System.out.println(data2.obterDataFormatada());
	}

	
	
	
}
