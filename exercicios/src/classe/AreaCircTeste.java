package classe;

public class AreaCircTeste {
	public static void main(String[] args) {
		AreaCirc a1 = new AreaCirc(10);
		System.out.println(a1.area());
		
		AreaCirc a2 = new AreaCirc(5);
		System.out.println(a2.area());
		
		System.out.println(AreaCirc.area(100)); //usando diretamente o m�todo da classe
		System.out.println(AreaCirc.PI); // imprimindo a constante de classe
		System.out.println(Math.PI); // imprimindo o PI diretamente da classe padr�o - Math.
	}
	
}
