package classe.desafio;

public class Pessoa {
	
	String nomeDaPessoa;
	double pesoDaPessoa;

	Pessoa(String nomeDaPessoa, double pesoDaPessoa){
		this.nomeDaPessoa = nomeDaPessoa;
		this.pesoDaPessoa = pesoDaPessoa;
	}
	
//	double Comer(String nomeDaComida, double peso){
//		return this.pesoDaPessoa += peso;
//		
//	}		
	
	void comer(Comida comida) {
		if(comida != null) {
			this.pesoDaPessoa += comida.pesoDaComida;
		}
	}
	
	String apresentarPessoa() {
		return "Ol�, eu sou o " + nomeDaPessoa + " e tenho " + pesoDaPessoa + " Kgs.";
	}
}
