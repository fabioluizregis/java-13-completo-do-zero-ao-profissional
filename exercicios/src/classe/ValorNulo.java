package classe;

public class ValorNulo {
	public static void main(String[] args) {
		String s1 = ""; //vazio
		System.out.println(s1.concat("!!!!!"));
		
		String s2 = null; //null precisa ser explicitado!
		System.out.println(s2.concat("???"));
		
		//nao podemos acessar nem atributo, nem m�todo de uma variavel ou objeto nulo.
	}
}
