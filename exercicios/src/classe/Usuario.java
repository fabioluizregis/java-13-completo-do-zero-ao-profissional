package classe;

public class Usuario {
	
	String nome;
	String email;
	
	public boolean equals(Object objeto) {

		if(objeto instanceof Usuario) {
			Usuario outro = (Usuario) objeto;  //Conceito de casting. Conversanto Objeto para tipo Usuario.
			
			boolean nomeIgual = outro.nome.equals(this.nome);
			boolean emailIgual = outro.email.equals(this.email);
			
			return nomeIgual && emailIgual;
		} else {
			return false;
		}
	}
	
	// O hashcode ser� abordado em outra aula!
	public int hashCode() {
		return 0;
	}
}
