package arrays;

import java.util.Scanner;

public class DesafioArray {
	public static void main(String[] args) {
		System.out.println("Ol�, bem vindo ao sistema de digita��o de notas para c�lculo de m�dias!");
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Quantas notas deseja digitar?");
		System.out.print("Resposta: ");
		int quantidadeDeNotas = entrada.nextInt();
	
		System.out.println("Obrigado por informar a quantidade de notas que ser�o digitadas!");
		System.out.println("Digite a separa��o de casas decimais com v�rgula. Ex: 8,9");
		
		double[] notas = new double[quantidadeDeNotas];
		
		for (int i = 0; i < notas.length; i++) {
			System.out.print("Ditite sua " + (i+1) + "o. nota: ");
			notas[i] = entrada.nextDouble();
		}
		
		double calculaMedia = 0;
		for(double nota: notas) {
			calculaMedia += nota;
		}
		
		System.out.println("A m�dia dessas notas �: " + (calculaMedia/notas.length) );
		
		entrada.close();
	}
}
