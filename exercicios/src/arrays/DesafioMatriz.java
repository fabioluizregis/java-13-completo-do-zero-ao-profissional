package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class DesafioMatriz {
	public static void main(String[] args) {
		System.out.println("Ol�, bem vindo ao sistema de digita��o de notas para c�lculo de m�dias!");
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Quantos alunos tem na sua classe?");
		System.out.print("Resposta: ");
		int quantidadeDeAlunos = entrada.nextInt();		
		System.out.println("Obrigado por informar a quantidade de alunos!");
		
		System.out.println("Quantas notas deseja digitar?");
		System.out.print("Resposta: ");
		int quantidadeDeNotas = entrada.nextInt();
		System.out.println("Obrigado por informar a quantidade de notas que ser�o digitadas!");
		System.out.println("Digite a separa��o de casas decimais com v�rgula. Ex: 8,9");
		
		double[][] notas = new double[quantidadeDeAlunos][quantidadeDeNotas];
		
		for (int i = 0; i < quantidadeDeAlunos; i++) {
			for (int j = 0; j < quantidadeDeNotas; j++) {
				
			System.out.print("Ditite sua " + (i+1) + "o. nota no aluno " + (j+1) + ":");
			notas[i][j] = entrada.nextDouble();
			}
		}
		
		double somaNotasClasse = 0;
		int contador = 0;
		for (int i = 0; i < quantidadeDeAlunos; i++) {
			for (int j = 0; j < quantidadeDeNotas; j++) {
				somaNotasClasse += notas[i][j];
				contador += 1;
			}
			
		}
		System.out.println("A m�dia dessas notas �: " + (somaNotasClasse/contador) );
		
		for(double[] notasDoAluno: notas)
		{
			System.out.println(Arrays.toString(notasDoAluno));
		}
		
		entrada.close();
	}
}
