package oo.composicao.desafio;

public class ClienteTeste {
	public static void main(String[] args) {
		
		Cliente cliente1 = new Cliente("Fabio");
		
		Compra compra1 = new Compra();
		compra1.adicionarItem("Leite", 3.20, 1);
		compra1.adicionarItem("Queijo", 12.50, 3);
		
		Compra compra2 = new Compra();
		compra2.adicionarItem("Farinha", 3.35, 2);
		compra2.adicionarItem("Ovo", 7.50, 1);
		
		cliente1.adicionarCompra(compra1);
		cliente1.compras.add(compra2);
		
		System.out.println(cliente1.obterValorTotal());
	}
}

