package oo.composicao;

public class Motor {

	//colocando relacao bidirecional para exemplo
	Carro carro;
	Motor(Carro carro){
		this.carro = carro;
	}
	
	boolean ligado = false;
	double fatorIngecao = 1;

	int giros() {
		if (!ligado) {
			return 0;
		} else {
			return (int) Math.round(fatorIngecao * 3000);
		}
	}
	
}
