# UDEMY - Java 13 COMPLETO: Do Zero ao Profissional + Projetos Reais!

Este repositório contém os exercícios, projetos e anotações sobre o curso realizado na Udemy.

## Instalação - Montando o ambiente necessário.

## Instalar o JAVA versão 13
    - Site: java.oracle.com
        - Baixar e instalar a versão: Java SE 13.0.2 (Java SE Development Kit 13 Downloads)
        - Não esqueça de configurar as variáveis de ambiente

## Instalar o Eclipse
    - Site: eclipse.org
        - Baixar e instalar a versão Eclipse IDE for Enterprise Java Developers

## ECLIPSE - ATALHOS ÚTEIS
    CRTL + Espaço = Abre caixa de sugestões no código
    CRTL + M = Exibe / Oculta menu lateral
    CRTL + SHIFT + F = Formata o código
    CRTL + SHIFT + F11 = Executa o código
    CRTL + / = Comenta/Descomenta trechos de código
    CRTL + SHIFT + o = Organiza os imports e importa o que estive faltando
    CRTL + SHIFT + L + L = Abre menu de teclas de atalho. (L pressiona 2X)

    ALT + SETA para cima ou para baixo = Move a linha onde está o cursor ou seleção
    CRTL + ALT + SETA para cima ou para baixo = Copia a linha onde está o cursor ou seleção